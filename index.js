const express = require('express');
const app = require('express')();
const fs = require('fs');
const path = require('path');

const serveIndex = require('serve-index');
var vidStreamer = require("vid-streamer");

var newSettings = {
	rootFolder: "/usr/src/",
	rootPath: "video/",
	forceDownload: false,
}
let index = serveIndex(path.resolve(__dirname), {
	'icons': true
});
app.use('/',(req,res)=>{
	index(req,res, ()=>{
		if(req.url.match(/.(mp4|mkv|wmv|flv|webm|gif|avi)$/)){
			res.writeHead(302, {
				'Location': `/video${req.url}`
	      		});
	      		return res.end();
		}
	});
});

app.get('/video/*',vidStreamer.settings(newSettings));
app.listen(80);
