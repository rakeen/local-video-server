# What

A file server that indexes files and streams videos.


# Why

I have my movies and podcasts in my laptop and often time I watch those while 
in bed. I realized holding a laptop while lying in bed gets strenuous after a while.

Using phone is a good alternative since it is much lighter. Since I have a wireless home
network I can easily access files of my laptop from my mobile.

I used to use python's `SimpleHTTPServer` but that requires some exception handling and
often times resulted in a Broken Pipe Error.

### Bit more about Broken Pipe

When a client program doesn't wait till all the data from the server is received the server
simply closes that connection. More specifically the `SimpleHTTPServer` process receives a 
`SIGPIPE` signal.

[SO reference](https://stackoverflow.com/a/11866962/4437655)


# How

I used `serve-index` to generate the indexes and arguably a nicely done UI and `vid-streamer` 
for streaming the videos.


